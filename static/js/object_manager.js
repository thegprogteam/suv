var ms;
	var om;
ymaps.ready(init);


	
function init () {
    var myMap = new ymaps.Map("mymaps", {
            center: [55.76, 37.64],
            zoom: 4
        }, {
            searchControlProvider: 'yandex#search'
        }),
    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    
	        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 10
        });
		
		ms = myMap;
		om = objectManager;
		objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    myMap.geoObjects.add(objectManager);

    $.ajax({
        url: "/json/build"
    }).done(function(data) {
        objectManager.add(data);
    });
	
	
}

function renew(mps, obj) {

	  $.ajax({
        url: "/json/build"
    }).done(function(data) {
        obj.add(data);
    });
}