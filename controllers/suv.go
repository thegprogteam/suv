package controllers

import (
	"container/list"
	models "suvto/models"
	//"time"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
)

func Join(GP GeoPosition, ws *websocket.Conn) int64 {
	beego.Error("New User Added")
	freeId := models.GetNextFreeId(GP.Alt, GP.Lat)
	subscribe <- Subscriber{Id: freeId, GP: GP, Conn: ws}
	return freeId
}

func Leave(id int64, ws *websocket.Conn) {
	beego.Error("User Leave")
	unsubscribe <- Subscriber{Id: id, Conn: ws}
}

type Subscriber struct {
	Id   int64
	GP   GeoPosition
	Conn *websocket.Conn // Only for WebSocket users; otherwise nil.
}

var (
	// Channel for new join users.
	subscribe = make(chan Subscriber, 10)
	// Channel for exit users.
	unsubscribe = make(chan Subscriber, 10)
	// Send events here to publish them.
	publish     = make(chan models.Event, 10)
	subscribers = list.New()
)

func newEvent(ep models.EventType, id int64) models.Event {
	info := models.GetInfoAboutId(id)
	return models.Event{ep, info}
}

func init() {
	go suv()
}

func suv() {
	for {
		select {
		case sub := <-subscribe:
			// Publish a JOIN event.
			subscribers.PushBack(sub)
			publish <- newEvent(models.EVENT_JOIN, sub.Id)
			beego.Info("New user:", sub.GP, sub.Id, ";WebSocket:", sub.Conn != nil)
		case event := <-publish:
			// Notify waiting list
			broadcastWebSocket(event)
		case unsub := <-unsubscribe:

			for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
				if sub.Value.(Subscriber).Id == unsub.Id {
					subscribers.Remove(sub)
					// Clone connection.
					ws := sub.Value.(Subscriber).Conn
					if ws != nil {
						ws.Close()
						beego.Error("WebSocket closed:", unsub)
						models.SetOffline(int(unsub.Id))
					}
					publish <- newEvent(models.EVENT_LEAVE, unsub.Id) // Publish a LEAVE event.
					break
				}
			}

		}
	}
}
