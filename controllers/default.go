package controllers

import (
	"encoding/json"
	"net/http"
	models "suvto/models"
	_ "time"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
}

type GeoPosition struct {
	Alt float64
	Lat float64
}

func (this *MainController) Join() {
	// Upgrade from http request to WebSocket.
	ws, err := websocket.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(this.Ctx.ResponseWriter, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		beego.Error("Cannot setup WebSocket connection:", err)
		return
	}

	// Join chat room.

	msg := GeoPosition{}
	err = ws.ReadJSON(&msg)

	/*_, p, err := ws.ReadMessage()
	if err != nil {
		return
	}*/
	if err != nil {
		beego.Error("Error reading json.", err)
		msg.Alt, msg.Lat = models.GEOInit(this.Ctx.Input.IP())
	}

	beego.Error("Got message: ", msg)

	id := Join(msg, ws)
	defer Leave(id, ws)

}

func broadcastWebSocket(event models.Event) {
	data, err := json.Marshal(event)
	if err != nil {
		beego.Error("Fail to marshal event:", err)
		return
	}

	//models.BuildGeo()
	//tickChan := time.NewTicker(time.Millisecond * 400).C

	for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
		/*select {
		case <-tickChan:
			ws := sub.Value.(Subscriber).Conn
			if ws != nil {
				if ws.WriteMessage(websocket.TextMessage, data) != nil {
					// User disconnected.
					unsubscribe <- sub.Value.(Subscriber)
				}
			}
		}*/
		// Immediately send event to WebSocket users.
		ws := sub.Value.(Subscriber).Conn
		if ws != nil {
			if ws.WriteMessage(websocket.TextMessage, data) != nil {
				// User disconnected.
				unsubscribe <- sub.Value.(Subscriber)
			}
		}
	}
}

type TestController struct {
	beego.Controller
}

func (t *TestController) Get() {
	data := models.BuildGeo()

	t.Data["json"] = data
	t.ServeJSON(false)
	//t.TplName = "json.tpl"
}
