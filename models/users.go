package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func GetNextFreeId(Lat float64, Lon float64) int64 {
	o := orm.NewOrm()
	us := Users{}
	us.Is_online = true
	us.Lat = Lat
	us.Lon = Lon

	id, err := o.Insert(&us)
	if err != nil {
		beego.Error(err)
	}
	return id
}

func SetOffline(id int) {
	o := orm.NewOrm()
	user := Users{Id: id}
	if o.Read(&user) == nil {
		user.Is_online = false
		if num, err := o.Update(&user, "is_online"); err == nil {
			beego.Error(num)
		}
	}
}

func SetOnline(id int) {
	o := orm.NewOrm()
	user := Users{Id: id}
	if o.Read(&user) == nil {
		user.Is_online = true
		if num, err := o.Update(&user, "is_online"); err == nil {
			beego.Error(num)
		}
	}
}

func GetInfoAboutId(id int64) Users {
	usr := Users{}
	usr.Id = int(id)

	o := orm.NewOrm()
	err := o.Read(&usr)

	if err == orm.ErrNoRows {
		beego.Error("No result found.")
	} else if err == orm.ErrMissPK {
		beego.Error("No primary key found.")
	}

	return usr
}
