package models

import (
	_ "github.com/astaxie/beego"
	_ "github.com/astaxie/beego/orm"
)

type EventType int

const (
	EVENT_JOIN = iota
	EVENT_LEAVE
)

type Event struct {
	Type EventType // JOIN, LEAVE, MESSAGE
	Usr  Users
}
