package models

import (
	//"encoding/json"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/lib/pq"
)

type Users struct {
	Id        int    `orm:"auto;column(id)"`
	Ip        string `orm:type(text)`
	Is_online bool   `orm:"column(is_online)"`
	Lon       float64
	Lat       float64
}

func init() {
	orm.RegisterDriver("postgres", orm.DRPostgres)
	orm.RegisterModel(new(Users))
	orm.RegisterDataBase("default", "postgres", "host=localhost port=5432 user=postgres password=1234 dbname=suv sslmode=disable")
}

type Data struct {
	Type     string    `json:"type"`
	Features []Feature `json:"features"`
}

type Feature struct {
	Type       string     `json:"type"`
	Id         int        `json:"id"`
	Geometry   Geometry   `json:"geometry"`
	Properties Properties `json:"properties"`
}

type Geometry struct {
	Type        string     `json:"type"`
	Coordinates [2]float64 `json:"coordinates"`
}

type Properties struct {
	BalloonContent string `json:"balloonContent"`
	ClusterCaption string `json:"clusterCaption"`
	HintContent    string `json:"hintContent"`
}

func BuildGeo() Data { //([]byte, error) {
	var Geos Data
	o := orm.NewOrm()
	var usr []*Users
	_, err := o.QueryTable("users").Filter("is_online", true).All(&usr)
	if err != nil {
		beego.Error(err)
		return Geos //[]byte(""), err
	}
	Geos.Type = "FeatureCollection"
	fts := make([]Feature, 0)

	for index, val := range usr {

		var ftsOne Feature
		ftsOne.Id = index
		ftsOne.Type = "Feature"
		var Geom Geometry
		Geom.Type = "Point"
		Geom.Coordinates[0] = val.Lat
		Geom.Coordinates[1] = val.Lon

		ftsOne.Geometry = Geom

		var PR Properties
		PR.BalloonContent = "Пользователь"
		PR.ClusterCaption = "Название кластера"
		PR.HintContent = "Клик"

		ftsOne.Properties = PR

		fts = append(fts, ftsOne)

	}

	Geos.Features = fts

	//data, err := json.Marshal(Geos)
	//beego.Error(string(data))
	return Geos
}
