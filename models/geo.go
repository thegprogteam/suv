package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
)

//ff231f6f70dd2d4fdbe212a012a35f56
//http://htmlweb.ru/geo/api.php?json&ip=IP_АДРЕС&charset=utf-8&api_key=ff231f6f70dd2d4fdbe212a012a35f56

func GEOInit(ip string) (float64, float64) {
	var dat map[string]interface{}
	//ip = parseIp(ip)
	url := "http://htmlweb.ru/geo/api.php?json&charset=utf-8&api_key=ff231f6f70dd2d4fdbe212a012a35f56&ip=" + ip
	req := httplib.Get(url)
	err := req.ToJSON(&dat)
	if err != nil {
		beego.Error(err)
	}
	return dat["latitude"].(float64), dat["longitude"].(float64)
}
