<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="/static/img/favicon.png" />
<meta name="author" content="Unknown" />
<meta name="description" content="Beego sample app - Web IM" />
<meta name="keywords" content="Go, golang, beego, sample, Web IM">

 <!-- Material Design fonts -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <!-- Bootstrap Material Design -->
  <link rel="stylesheet" type="text/css" href="static/css/bootstrap-material-design.css">
  <link rel="stylesheet" type="text/css" href="static/css/ripples.min.css">

    <script src="//yandex.st/jquery/2.1.0/jquery.min.js" type="text/javascript"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
    <script src="/static/js/object_manager.js" type="text/javascript"></script>
	
<script src="/static/js/websocket.js"></script>

</script>
	<style>
       html, body  {
            width: 100%; height: 100%; padding: 0; margin: 0;
        }
		#mymaps{
			 width: 75%; height: 75%; padding: 0; margin: 0;
		}
    </style>
	</head>
<body>


        <div class="navbar navbar-default">
            <div class="container-fluid">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="javascript:void(0)">SUserView</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="javascript:void(0)">Регионы</a></li>
            <li><a href="javascript:void(0)">Дата-центры</a></li>

        </ul>
        <form class="navbar-form navbar-left">
            <div class="form-group">
            <input type="text" class="form-control col-lg-10 col-md-10 col-xs-8" placeholder="Поиск по региону">
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="javascript:void(0)">Статистика</a></li>
            <li class="dropdown">
            <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Быстрый переход
                <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="javascript:void(0)">Action</a></li>
                <li><a href="javascript:void(0)">Another action</a></li>
                <li><a href="javascript:void(0)">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="javascript:void(0)">Separated link</a></li>
            </ul>
            </li>
        </ul>
        </div>
    </div>
    </div>



  <div id="mymaps"></div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="static/js/ripples.min.js"></script>
<script src="static/js/material.min.js"></script>
<script type="text/javascript">$.material.init() </script>
</body>
</html>
