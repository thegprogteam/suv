package routers

import (
	"suvto/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/ws/join", &controllers.MainController{}, "get:Join")
	beego.Router("/json/build", &controllers.TestController{})
}
